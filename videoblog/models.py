from django.db import models
from django.shortcuts import reverse
from django.utils import timezone
from django.contrib.auth.models import User


class Profile(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=200, null=True)
    phone = models.CharField(max_length=200, null=True)
    email = models.CharField(max_length=200, null=True)
    profile_pic = models.ImageField(null=True, blank=True)
    date_created = models.DateTimeField(auto_now_add=True, null=True)

    class Meta:
        verbose_name = "Профиль"

    def __str__(self):
        return self.name


class Video(models.Model):
    title = models.CharField('Название', max_length=255)
    video = models.FileField('Видео', upload_to='videos/')

    class Meta:
        verbose_name = "Видео"
        verbose_name_plural = "Видео"

    # def get_absoulte_url(self):
    #     return reverse('video_detail_url', kwargs={'id':self.id})

    def __str__(self):
        return self.title


class Image(models.Model):
    title = models.CharField('Название', max_length=255)
    image = models.ImageField('Картинка', upload_to='images/')

    class Meta:
        verbose_name = "Картинка"
        verbose_name_plural = "Картинка"

    def __str__(self):
        return self.title


class Category(models.Model):
    title = models.CharField('Название', max_length=255)
    slug = models.SlugField('Ссылка', unique=True)
    image = models.ForeignKey(
        Image, on_delete=models.CASCADE, verbose_name="Картинка")

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"

    def get_absolute_url(self):
        return reverse('category_detail_url', kwargs={'slug': self.slug})

    def __str__(self):
        return self.title


class Post(models.Model):
    author = models.ForeignKey(
        User, on_delete=models.CASCADE, default=1, verbose_name='Автор')
    title = models.CharField('Название', max_length=255)
    slug = models.SlugField('Ссылка', unique=True)
    video = models.OneToOneField(
        Video, on_delete=models.CASCADE, verbose_name="Посты")
    image = models.ForeignKey(
        Image, on_delete=models.CASCADE, verbose_name="Картинка")
    content = models.TextField('Описание')
    categories = models.ManyToManyField(
        Category, related_name='posts', verbose_name="Категории")
    date = models.DateTimeField('Дата', default=timezone.now)
    views = models.IntegerField('Просмотри', default=0)

    class Meta:
        verbose_name = "Пост"
        verbose_name_plural = "Посты"

    def get_absolute_url(self):
        return reverse('post_detail_url', kwargs={'slug': self.slug})

    def __str__(self):
        return self.title

    def snippet(self):
        return self.content[:10]+'...'


class Comment(models.Model):
    post = models.ForeignKey(
        Post, on_delete=models.CASCADE, related_name='comments')
    user = models.ForeignKey(
        User, null=True, on_delete=models.CASCADE, related_name='comments')
    comment_text = models.TextField('Текст комментария')
    date = models.DateTimeField('Дата', default=timezone.now)

    class Meta:
        verbose_name = "Комментарий"
        verbose_name_plural = "Комментарии"

    def __str__(self):
        return self.user.username


class Like(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             related_name="likes", verbose_name='Пользователь')
    post = models.ForeignKey(Post, null=True, on_delete=models.CASCADE,
                             related_name="likes", verbose_name="Ролик")
    status = models.BooleanField('Активность', default=True)
    date = models.DateTimeField('Дата', default=timezone.now)

    class Meta:
        verbose_name = "Like"
        verbose_name_plural = "Like"

    def __str__(self):
        return self.user.username + ' | ' + self.post.title


class Dislike(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             related_name="dislikes", verbose_name='Пользователь')
    post = models.ForeignKey(Post, null=True, on_delete=models.CASCADE,
                             related_name="dislikes", verbose_name="Ролик")
    status = models.BooleanField('Активность', default=True)
    date = models.DateTimeField('Дата', default=timezone.now)

    class Meta:
        verbose_name = "Dislike"
        verbose_name_plural = "Dislike"

    def __str__(self):
        return self.user.username + ' | ' + self.post.title
