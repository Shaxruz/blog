from django import forms
from .models import Profile, Post
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm

class RegisterForm(UserCreationForm):
    username = forms.CharField(label='Никнайм', required=True, widget=forms.TextInput(attrs={'class':'form-control footer-input margin-b-20', 'placeholder': 'Ваш никнейм'}))
    password1 = forms.CharField(label='Пароль', required=True, widget=forms.PasswordInput(attrs={'class':'form-control footer-input margin-b-20', 'placeholder': 'Пароль должен быть сложным!'}))
    password2 = forms.CharField(label='Потврерждение', required=True, widget=forms.PasswordInput(attrs={'class':'form-control footer-input margin-b-20', 'placeholder': 'Повторите пароль'}))
    email = forms.EmailField(label='Email', required=True, widget=forms.TextInput(attrs={'class':'form-control footer-input margin-b-20', 'placeholder': 'Ваш Email'}))
    first_name = forms.CharField(label='Имя', required=True, widget=forms.TextInput(attrs={'class':'form-control footer-input margin-b-20', 'placeholder': 'Ваше реальное Имя'}))
    last_name = forms.CharField(label='Фамилия', required=True, widget=forms.TextInput(attrs={'class':'form-control footer-input margin-b-20', 'placeholder': 'Ваша Фамилия'}))

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'username', 'password1', 'password2',)

    def save(self, commit=True):
        user = super().save(commit=False)
        user.email = self.cleaned_data['email']
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        if commit:
            user.save()
            return user

class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = '__all__'
        exclude = ['user']

class PostCreate(forms.ModelForm):
    class Meta:
        model = Post
        fields = '__all__'



# class CreatePost(forms.ModelForm):
#     class Meta:
#         model = models.Post
#         fields = ['title','video','image','content','categories']


# class VideoCreate(forms.ModelForm):

#     class Meta:
#         model = models.Video
#         fields = ('title', 'video',)
        


# class CreateImage(forms.ModelForm):
#     class Meta:
#         model = models.Image
#         fields = ['title','image']