# Generated by Django 3.0.5 on 2020-05-14 13:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('videoblog', '0010_auto_20200510_0049'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='dislike',
            options={'verbose_name': 'Dislike', 'verbose_name_plural': 'Dislike'},
        ),
        migrations.AlterModelOptions(
            name='like',
            options={'verbose_name': 'Like', 'verbose_name_plural': 'Like'},
        ),
        migrations.AlterModelOptions(
            name='profile',
            options={'verbose_name': 'Профиль'},
        ),
    ]
