from django.urls import path
from . import views
from .models import Like, Dislike, Post
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('', views.index, name="index"),
    path('sort/', views.sort, name="sort"), 
    path('posts/<slug:slug>/', views.post_detail, name="post_detail_url"),
    path('posts/<slug:slug>/like/', views.like, name="like"),
    path('posts/<slug:slug>/dislike/', views.dislike, name="dislike"),
    path('posts/<slug:slug>/leave-comment', views.leave_comment, name="leave_comment"),
    path('register/', views.register, name="register"),
    path('login/', views.login, name="login"),
    path('profile/', views.profile, name="profile"),
    path('search/', views.search, name = "search"),
    path('categories/<slug:slug>/', views.category_detail, name="category_detail_url"),
    path('upload/', views.upload, name = 'upload'),
    path('update/<int:post_id>', views.update_post),
    path('delete/<int:post_id>', views.delete_post)
]