from django.shortcuts import render, reverse, redirect
from .models import Video, Image, Category, Post, Comment, Like, Dislike, Profile
from .import forms 
from django.core.files.storage import FileSystemStorage
import json
from django.views import View
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponse
from .forms import RegisterForm, ProfileForm, PostCreate
from django.db.models import Q
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required




# Create your views here.

def index(request):
    posts = Post.objects.all()
    return render(request, 'index.html', {'posts':posts})

def upload(request):
    upload = PostCreate()
    if request.method == 'POST':
        upload = PostCreate(request.POST, request.FILES)
        if upload.is_valid():
            upload.save()
            return redirect('index')
        else:
            return HttpResponse("""your form is wrong, reload on <a href = "{{url : 'index'}}">reload</a>""")
    else:
        return render(request, 'upload.html', {'upload':upload})

def update_post(request, post_id):
    post_id = int(post_id)
    try:
        post_sel = Post.objects.get(id = post_id)
    except Post.DoesNotExist:
        return redirect('index')
    post_form = PostCreate(request.POST or None, instance = post_sel)
    if post_form.is_valid():
       post_form.save()
       return redirect('index')
    return render(request, 'upload.html', {'upload':post_form})

def delete_post(request, post_id):
    post_id = int(post_id)
    try:
        post_sel = Post.objects.get(id = post_id)
    except Post.DoesNotExist:
        return redirect('index')
    post_sel.delete()
    return redirect('index')



def register(request):
    if not request.user.is_authenticated:
        if request.method == 'POST':
            form = RegisterForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect('login')
        else:
            form = RegisterForm()
            return render(request, 'register.html', {'form':form} )

    return redirect('index')
   
def login(request):
    #user = request.profile
    form = ProfileForm()
    return render(request, 'login.html', {'form':form})

@login_required(login_url='login')
#@allowed_users(allowed_roles=['profile']) #models.py
def profile(request):
    profile = request.user.profile
    form = ProfileForm(instance=profile)

    if request.method == 'POST':
         form = ProfileForm(request.POST, request.FILES, instance=profile)
         if form.is_valid():
             form.save()
         
    return render(request, 'profile.html', {'form':form})

def sort(request):
    if request.method == 'POST':
        value = request.POST.get('sort')
        posts = Post.objects.order_by(value)
        return render(request, 'sort.html', {'value':value,'posts':posts})
    return redirect('index')

def post_detail(request, slug):
    post = Post.objects.get(slug__exact = slug)
    post.views +=1
    post.save()
    
    return render(request, 'post_detail.html', {'post':post}, )

def category_detail(request, slug):
    category = Category.objects.get(slug__exact = slug)
    return render(request, 'category_detail.html', {'category':category})

def search(request):
    query = request.GET.get('search')
    search_obj = Post.objects.filter(
        Q(title__icontains = query) | Q(content__icontains = query)
    )
    return render(request, 'search.html', {'search_obj':search_obj, 'query':query})


def leave_comment(request, slug):
    if request.method == 'POST':
        post = Post.objects.get(slug__exact = slug)
        post.comments.create(user = request.user, comment_text = request.POST.get('text'))
        return redirect(reverse('post_detail_url', args=(slug,) ))
    return redirect('index')

def like(request, slug):
    post = Post.objects.get(slug__iexact = slug)
    if request.method == 'POST':
        if post.likes.filter(user = request.user).exists():
            like = post.likes.get(user = request.user)
            if like.status:
                like.status = False
            else:
                like.status = True
                if post.dislikes.filter(user = request.user).exists():
                    dislike = post.dislikes.get(user = request.user)
                    dislike.status = False
                    dislike.save() 
            like.save()       
            return redirect(reverse('post_detail_url', args= (slug, )))
        else:    
            post.likes.create(user = request.user) 
            return redirect(reverse('post_detail_url', args= (slug, )))  
    return redirect('index')

def dislike(request, slug):
    post = Post.objects.get(slug__iexact = slug)
    if request.method == 'POST':
        if post.dislikes.filter(user = request.user).exists():
            dislike = post.dislikes.get(user = request.user)
            if dislike.status:
                dislike.status = False
            else:
                dislike.status = True
                if post.likes.filter(user = request.user).exists():
                    like = post.likes.get(user = request.user)
                    like.status = False
                    like.save() 
            dislike.save()
            return redirect(reverse('post_detail_url', args= (slug, )))
        else:
            post.dislikes.create(user = request.user)  
            return redirect(reverse('post_detail_url', args= (slug, )))     
    return redirect('index')





def create(request):
    form = forms.CreatePost()
    return render(request, 'create.html', {'form':form})

def createvideo(request):
    form = forms.VideoCreate()
    if request.method == 'POST':
        # video = Video()
        # video.title = request.POST.get('title')
        # myfile = request.FILES['video']
        # fs = FileSystemStorage()
        # filename = fs.save(myfile.name, myfile)
        # video.video = fs.url(filename)
        form = forms.VideoCreate(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('index')

    return render(request, 'video.html', {'form':form})

def createimage(request):
    image = forms.CreateImage()
    return render(request, 'image.html', {'image':image})