from django.contrib import admin
from .models import Video, Image, Post, Category, Comment, Profile

class SlugAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug':('title',)}


admin.site.register(Post)
admin.site.register(Video)
admin.site.register(Image)
admin.site.register(Category)
admin.site.register(Comment)
admin.site.register(Profile)

# Register your models here.
